#!/usr/bin/env python3

import os
from pathlib import Path
import subprocess
import tarfile
import tkinter as tk
from tkinter import filedialog 
import urllib.request
import zipfile
from zipfile import ZipFile

try:
    from consolemenu import *
    from consolemenu.items import *
except ModuleNotFoundError:
    print("`console-menu` module is missing")
    print("Use 'py -m pip install console-menu'")
    exit(1)

try:
    import progressbar
except ModuleNotFoundError:
    print("`progressbar` module is missing")
    print("Use `py -m pip install progressbar`")
    exit(1)

def run_cmd_with_log(cmd, check=False, cwd=None):
    print(f'> {cmd}')
    subprocess.run(cmd, check=check, cwd=cwd, shell=True)

def zipdir(zip_file, path, arc_path=None):
    assert path.exists() and path.is_dir()

    for file in path.rglob('*'):
        if file.name.startswith('.'):
            # skip `hidden` files
            continue
        if arc_path:
            file_arc_path = f'{arc_path}/{file.relative_to(path)}'
        else:
            file_arc_path = file.relative_to(path)
        zip_file.write(file, file_arc_path)


def unixify_path(path):
    return '/' + str(path).replace('\\', '/').replace(':', '') 

def get_cwd_repo_root():
    repo_dir = Path(os.getcwd()).absolute()
    cmd_get_root = ['git', '-C', str(repo_dir), 'rev-parse', '--show-toplevel']

    root = subprocess.check_output(
        cmd_get_root, text=True, env=os.environ, cwd=repo_dir
    ).strip()
    return Path(root)


def get_version(version_file: Path):
    if not version_file.exists():
        raise ValueError(f'Repo is missing {version_file} file')

    with open(version_file, 'r') as input:
        version = input.read().strip()

    return version


def ask_working_dir():
    root = tk.Tk()
    root.withdraw()
    return filedialog.askdirectory(title='Select working directory for building')


def work_dir_item_wrapper(fn):
    def impl(*args, **kwargs):
        global current_settings
        work_dir = current_settings['work_dir']
        if not work_dir:
            input('Set up the working directory first')
            return

        fn(*args, **kwargs)
        print('')
        input('Press any key to continue...')
    
    return impl


def download_progress_bar_fn(block_num, block_size, total_size):
    global pbar
    if pbar is None:
        pbar = progressbar.ProgressBar(maxval=total_size)
        pbar.start()

    downloaded = block_num * block_size
    if downloaded < total_size:
        pbar.update(downloaded)
    else:
        pbar.finish()
        pbar = None


def extract_progress_bar_fn(current, total_count):
    global pbar
    if pbar is None:
        pbar = progressbar.ProgressBar(maxval=total_count)
        pbar.start()

    if current < total_count:
        pbar.update(current)
    else:
        pbar.finish()
        pbar = None


def tar_member_wrapper(members):
    members_count = len(members)
    for i, member in enumerate(members, start=1):
        # this will be the current file being extracted
        yield member
        extract_progress_bar_fn(i, members_count)
    extract_progress_bar_fn(members_count, members_count)


def get_src_path(work_dir):
    return work_dir/f'firefox-{firefox_version}'.removesuffix('esr')


current_settings = {'work_dir': None}
root_dir = get_cwd_repo_root()
scripts_dir = root_dir/'build_scripts'
firefox_version = get_version(scripts_dir/'FIREFOX_VERSION')
mozilla_build_version = get_version(scripts_dir/'MOZILLABUILD_VERSION')
use_nspr = 1 if firefox_version.startswith('91.') else 0


change_working_dir_item = MenuItem('Working directory: [not set]')

def change_working_dir_fn():
    work_dir = ask_working_dir()
    if work_dir:
        current_settings['work_dir'] = Path(work_dir)
        change_working_dir_item.text = f'Working directory: `{work_dir}`'


change_working_dir_item.action = change_working_dir_fn


@work_dir_item_wrapper
def download_mozbuild_fn():
    work_dir: Path = current_settings['work_dir']
    url = f'https://ftp.mozilla.org/pub/mozilla/libraries/win32/MozillaBuildSetup-{mozilla_build_version}.exe'
    file = work_dir/Path(url).name

    print('If you want to abort the process, close the terminal')
    print('')
    print(f'>> Downloading `{url}`...')
    urllib.request.urlretrieve(url, file, download_progress_bar_fn)
    print('<< Downloaded')
    print('')
    print(f'Install MozillaBuild to `{work_dir/"mozilla-build"}`')
    print('')
    input('Press any key to continue...')

    subprocess.run(file, env=os.environ)


@work_dir_item_wrapper
def download_sources_fn():
    work_dir: Path = current_settings['work_dir']
    url = f'https://ftp.mozilla.org/pub/firefox/releases/{firefox_version}/source/firefox-{firefox_version}.source.tar.xz'
    file = work_dir/Path(url).name

    print('If you want to abort the process, close the terminal')
    print('')
    print(f'>> Downloading `{url}`...')
    urllib.request.urlretrieve(url, file, download_progress_bar_fn)
    print('<< Downloaded')
    print('')
    print(f'>> Extracting `{file}`...')
    with tarfile.open(file, 'r:xz') as f:
        f.extractall(path=work_dir, members=tar_member_wrapper(f.getmembers()))
    print('<< Extracted')


@work_dir_item_wrapper
def apply_patches_fn():
    work_dir: Path = current_settings['work_dir']
    sources_dir = get_src_path(work_dir)
    patches_dir = scripts_dir/'patches'/firefox_version.split('.')[0]
    patches = [i for i in patches_dir.iterdir() if i.is_file()]
    patches_arg = f'--ignore-whitespace {" ".join(str(p) for p in patches)}'
    
    print(f'>> Patching sources `{sources_dir}`...')
    try:
        run_cmd_with_log(f'git apply --quiet {patches_arg}', check=True, cwd=sources_dir)
        print('<< Patched')
    except:
        run_cmd_with_log(f'git apply --quiet -R {patches_arg}', cwd=sources_dir)
        run_cmd_with_log(f'git apply --check --verbose {patches_arg}', cwd=sources_dir)
        print('<< Patching failed')

@work_dir_item_wrapper
def configure_fn():
    work_dir: Path = current_settings['work_dir']
    mozillabuild_path = work_dir/'mozilla-build'/'start-shell.bat'
    sources_dir = get_src_path(work_dir)
    script_path = scripts_dir/'cmd'/'configure.sh'

    print(f'>> Configuring sources `{sources_dir}`...')    
    run_cmd_with_log(f'{mozillabuild_path} {unixify_path(script_path)} {unixify_path(sources_dir)} {use_nspr}', check=True)
    print('<< Configured')

@work_dir_item_wrapper
def build_fn(is_debug):
    work_dir: Path = current_settings['work_dir']
    mozillabuild_path = work_dir/'mozilla-build'/'start-shell.bat'
    sources_dir = get_src_path(work_dir)
    script_path = scripts_dir/'cmd'/'build.sh'

    print(f'>> Building `{sources_dir}`...')
    run_cmd_with_log( f'{mozillabuild_path} {unixify_path(script_path)} {unixify_path(sources_dir)} {int(is_debug)}', check=True)
    print('<< Built')


@work_dir_item_wrapper
def pack_dist_fn():
    work_dir:Path = current_settings['work_dir']
    if not current_settings['work_dir']:
        return

    output_dir = work_dir/'dist'
    output_dir.mkdir(parents=True, exist_ok=True)
    output_file = output_dir/f'mozjs_{firefox_version}.zip'
    output_file.unlink(missing_ok=True)

    js_dir = get_src_path(work_dir)/'js'/'src'

    print(f'>> Packing `{output_file}`...')
    with ZipFile(
        output_file, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9
    ) as z:
        for build_subdir, out_subdir in [('_DEBUG', 'Debug'), ('_RELEASE', 'Release')]:
            build_dir = js_dir/build_subdir
            zipdir(z, build_dir/'dist'/ 'include', f'{out_subdir}/include')
            for f in (build_dir/'dist'/'bin').glob('*.dll'):
                z.write(f, f'{out_subdir}/bin/{Path(f).name}')
            for f in (build_dir/'js'/'src'/'build').glob('mozjs-*.pdb'):
                z.write(f, f'{out_subdir}/bin/{Path(f).name}')
            for f in (build_dir/'js'/'src'/'build').glob('mozjs-*.lib'):
                z.write(f, f'{out_subdir}/lib/{Path(f).name}')
            if use_nspr:
                pdb_files = [
                    'config/external/nspr/ds/plds4.pdb',
                    'config/external/nspr/pr/nspr4.pdb',
                    'config/external/nspr/libc/plc4.pdb'
                ]
                for f in pdb_files:
                    z.write(build_dir/f,  f'{out_subdir}/bin/{Path(f).name}')
    print(f'Generated file: {output_file}')
    print('')
    input('Press any key to continue...')


download_mozbuild_item = FunctionItem(
    'Download and install MozillaBuild', download_mozbuild_fn
)
download_sources_item = FunctionItem(
    'Download and extract SpiderMonkey sources', download_sources_fn
)
apply_patches_item = FunctionItem('Apply patches', apply_patches_fn)
configure_item = FunctionItem('Configure build', configure_fn)
build_debug_item = FunctionItem('Build (debug)', build_fn, kwargs={'is_debug': True})
build_release_item = FunctionItem(
    'Build (release)', build_fn, kwargs={'is_debug': False}
)
pack_dist_item = FunctionItem('Pack dist', pack_dist_fn)

menu = ConsoleMenu('SpiderMonkey library build helper')
menu.append_item(change_working_dir_item)
menu.append_item(download_mozbuild_item)
menu.append_item(download_sources_item)
menu.append_item(apply_patches_item)
menu.append_item(configure_item)
menu.append_item(build_debug_item)
menu.append_item(build_release_item)
menu.append_item(pack_dist_item)

menu.show()
