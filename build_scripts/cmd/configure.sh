#!/bin/sh

set +x

SRC_DIR=$1
USE_NSPR=$2
LLVM_PATH="/c/Program Files/LLVM/bin"
if ! [[ -d "$LLVM_PATH" ]]; then
    if ! LLVM_PATH=$(where clang-cl.exe); then
       echo "Error: LLVM not found: install it or change it's path in this file"; exit 1
    fi
fi

BUILD_ARGS="--disable-jemalloc "
BUILD_ARGS+="--disable-js-shell "
BUILD_ARGS+="--disable-tests "
BUILD_ARGS+="--target=i686-pc-mingw32 "
BUILD_ARGS+="--host=i686-pc-mingw32 "
if [[ $USE_NSPR == 1 ]]; then
    BUILD_ARGS+="--enable-nspr-build "
else
    BUILD_ARGS+="--disable-nspr-build "
fi

set -x

rustup default stable-i686-pc-windows-msvc || exit 1

cd $SRC_DIR/js/src
cp ./configure.in ./configure
chmod +x ./configure

mkdir _RELEASE
cd _RELEASE
(\
    export PATH=$PATH:$LLVM_PATH && \
    export JS_STANDALONE=1 && \
    ../configure \
    $BUILD_ARGS \
) || exit 1

cd ..
mkdir _DEBUG
cd _DEBUG
(\
    export PATH=$PATH:"/c/Program Files/LLVM/bin/" && \
    export JS_STANDALONE=1 && \
    ../configure \
    $BUILD_ARGS \
    --enable-debug \
) || exit 1
