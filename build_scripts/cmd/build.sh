#!/bin/sh

set -x

SRC_DIR=$1
BUILD_DIR=$([[ "$2" == 1 ]] && echo "_DEBUG" || echo "_RELEASE")

cd $SRC_DIR/js/src/$BUILD_DIR
mozmake -j4
